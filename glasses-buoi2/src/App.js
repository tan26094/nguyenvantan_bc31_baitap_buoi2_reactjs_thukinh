import logo from "./logo.svg";
import "./App.css";
import Header_Title from "./Header/Header_Title";
import PageContent from "./PageContent/PageContent";
function App() {
  return (
    <div className="App">
      <Header_Title />
      <PageContent />
    </div>
  );
}

export default App;
