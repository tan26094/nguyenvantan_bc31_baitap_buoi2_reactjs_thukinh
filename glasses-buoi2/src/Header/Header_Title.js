import React, { Component } from "react";
import styles from "./HeaderStyles.module.css";
class Header_Title extends Component {
  render() {
    return (
      <div className="position-fixed w-100">
        <h1 className={styles.HeaderStyle}>TRY GLASS APP ONLINE</h1>
      </div>
    );
  }
}

export default Header_Title;
