import React, { Component } from "react";
import styles from "./pageContentCSS.module.css";
import { kinhArr } from ".././EX_glasses/dataGlasses";
class PageContent extends Component {
  kinhData = {
    listKinh: kinhArr,
  };
  state = {
    urlKinh: "",
  };
  renderListKinh = () => {
    return this.kinhData.listKinh.map((item) => {
      return (
        <div className="px-4 py-1 col-2 ">
          <img
            class="w-100"
            src={item.url}
            id={item.id}
            onClick={() => {
              this.pickupGlass(item.url);
            }}
          ></img>
        </div>
      );
    });
  };
  pickupGlass = (urls) => {
    console.log(urls);
    this.setState({ url: urls });
  };
  render() {
    return (
      <div className={styles.pageContentCSS}>
        <div>
          <div className={styles.card_model}>
            <img className={styles.img} src={this.state.url} alt=""></img>
          </div>
          <div className="d-flex p-0 flex-wrap text bg-dark container mx-auto align-items-center">
            {this.renderListKinh()}
          </div>
        </div>
      </div>
    );
  }
}

export default PageContent;
